package com.satgy.ventas;

import com.satgy.ventas.general.FileStorageProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication
@EnableConfigurationProperties({
		FileStorageProperties.class
})
public class VentasApplication {

    private static final Logger log = LoggerFactory.getLogger(VentasApplication.class);
    
    public static void main(String[] args) {
        log.debug("Este es el juego de don pirulero que todos queremos jugar");
        log.error("Las manos arriba, las manos abajo un golpe adelante y atrás");
        SpringApplication.run(VentasApplication.class, args);
    }

}

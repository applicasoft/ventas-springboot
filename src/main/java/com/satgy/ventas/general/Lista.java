package com.satgy.ventas.general;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Spliterator;
import java.util.function.Consumer;

public class Lista<K, V> implements Iterable<MiEntry<K, V>>{
    ArrayList <MiEntry<K, V>> lista;

    public Lista(){
        lista = new ArrayList<>();
    }
    
    public void add(K key, V value){
        lista.add(new MiEntry<>(key, value));
    }
    
    public void clear(){
        lista.clear();
    }
    
    public MiEntry<K, V> get(int i){
        return lista.get(i);
    }
    
    public K getKey(int i){
        return lista.get(i).getKey();
    }
    
    public V getValue(int i){
        return lista.get(i).getValue();
    }
    
    public int size(){
        return lista.size();
    }
    
    public MiEntry<K, V> remove(int i){
        return lista.remove(i);
    }
    
    public boolean remove(Object o){
        return lista.remove(o);
    }
    
    /**
     * Elimina todas las posiciones en que se encuentre una llave
     * @param key 
     */
    public void removeKey(K key){
        if (lista.size() > 0){
            for (int i=lista.size() -1 ; i>=0 ; i--) {
                if (lista.get(i).getKey().equals(key)) {
                    lista.remove(i);
                }
            }
        }
    }
    
    /**
     * un arreglo<Integer> con todas las ocurrencias donde se encontró la llave
     * @param key la llave a buscar
     * @return 
     */
    public ArrayList <MiEntry<K, V>> getKey(K key){
        ArrayList <MiEntry<K, V>> encontrados = new ArrayList<>();
        
        if (lista.size() > 0){
            for (int i=0 ; i<lista.size() ; i++) {
                if (lista.get(i).getKey().equals(key)) {
                    encontrados.add(lista.get(i));
                }
            }
        }
        return encontrados;
    }
    
    /**
     * un arreglo<Integer> con todas las ubicaciones donde se encontró la llave
     * @param key la llave a buscar
     * @return 
     */
    public ArrayList <Integer> findKey(K key){
        ArrayList <Integer> encontrados = new ArrayList();
        if (lista.size() > 0){
            for (int i=0 ; i<lista.size() ; i++) {
                if (lista.get(i).getKey().equals(key)) {
                    encontrados.add(i);
                }
            }
        }
        return encontrados;
    }
    
    /**
     * devuelve el primer valor que se encuentre
     * @param key la llave a buscar
     * @return null si no lo encuentra
     */
    public V getValue(K key){
        if (lista.size() > 0){
            for (int i=0 ; i<lista.size() ; i++) {
                if (lista.get(i).getKey().equals(key)) {
                    return getValue(i);
                }
            }
        }
        return null;
    }
    
    /**
     * devuelve la primera ubicación en que se encuentre la llave
     * @param key la llave a buscar
     * @return -1 si no lo encuentra
     */
    public int indexOf(K key){
        if (lista.size() > 0){
            for (int i=0 ; i<lista.size() ; i++) {
                if (lista.get(i).getKey().equals(key)) {
                    return i;
                }
            }
        }
        return -1;
    }
    
    /**
     * 
     * @param key la llave a buscar
     * @return true si se encuentra
     */
    public boolean existe(K key){
        return findKey(key).size() > 0;
    }
    
    @Override
    public Iterator<MiEntry<K, V>> iterator() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void forEach(Consumer<? super MiEntry<K, V>> action) {
        Iterable.super.forEach(action); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Spliterator<MiEntry<K, V>> spliterator() {
        return Iterable.super.spliterator(); //To change body of generated methods, choose Tools | Templates.
    }
    

}

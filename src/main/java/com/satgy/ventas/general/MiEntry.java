package com.satgy.ventas.general;

import java.util.Map.Entry;

public class MiEntry<K, V> implements Entry<K, V> {
    private final K key;
    private V value;
    public MiEntry(final K key) {
        this.key = key;
    }
    public MiEntry(final K key, final V value) {
        this.key = key;
        this.value = value;
    }
    public K getKey() {
        return key;
    }
    public V getValue() {
        return value;
    }
    public V setValue(final V value) {
        final V oldValue = this.value;
        this.value = value;
        return oldValue;
    }
}
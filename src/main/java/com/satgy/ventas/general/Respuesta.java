package com.satgy.ventas.general;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.Query;
import org.springframework.data.domain.Page;

public class Respuesta {
    int totalPaginas;
    int tamanoPagina; // items por página
    long totalItems;
    int numeroPagina; // pagina actual
    Object contenido;
    
    private static Respuesta crear(int totalPaginas, int tamanoPagina, long totalItems, int numeroPagina, Object contenido){
        return new Respuesta(totalPaginas, tamanoPagina, totalItems, numeroPagina, contenido);
    }
    
    public static Respuesta crearDePaged(Page pagedResult, int tamanoPagina, int numeroPagina){
        if(pagedResult.hasContent()) {
            return Respuesta.crear(pagedResult.getTotalPages(), tamanoPagina, pagedResult.getTotalElements(), numeroPagina, pagedResult.getContent());
        } else {
            return Respuesta.crear(pagedResult.getTotalPages(), tamanoPagina, pagedResult.getTotalElements(), numeroPagina, new ArrayList<>());
            //return Respuesta.crear(0, 0, 0, 0, new ArrayList<Producto>());
        }
    }
    
    public static Respuesta crearDeQuery(int pageSize, int pageNo, Query q){
        List tmp = q.getResultList(); 
        int totalItems = tmp.size();
        
        
        int totalPaginas = totalItems / pageSize;
        
        if (totalItems % pageSize > 0) totalPaginas ++;
        
        if (pageNo >= totalPaginas || pageNo < 0){
            return Respuesta.crear(totalPaginas, pageSize, totalItems, pageNo, new ArrayList());
        } else {
            int ini = pageNo * pageSize;
            int fin = pageNo * pageSize + pageSize;
            if (fin > totalItems) fin = totalItems;
            return Respuesta.crear(totalPaginas, pageSize, totalItems, pageNo, tmp.subList(ini, fin));
        }
    }

    public Respuesta(int totalPaginas, int tamanoPagina, long totalItems, int numeroPagina, Object contenido) {
        this.totalPaginas = totalPaginas;
        this.tamanoPagina = tamanoPagina;
        this.totalItems = totalItems;
        this.numeroPagina = numeroPagina;
        this.contenido = contenido;
    }

    public int getTotalPaginas() {
        return totalPaginas;
    }

    public void setTotalPaginas(int totalPaginas) {
        this.totalPaginas = totalPaginas;
    }

    public int getTamanoPagina() {
        return tamanoPagina;
    }

    public void setTamanoPagina(int tamanoPagina) {
        this.tamanoPagina = tamanoPagina;
    }

    public long getTotalItems() {
        return totalItems;
    }

    public void setTotalItems(long totalItems) {
        this.totalItems = totalItems;
    }

    public int getNumeroPagina() {
        return numeroPagina;
    }

    public void setNumeroPagina(int numeroPagina) {
        this.numeroPagina = numeroPagina;
    }

    public Object getContenido() {
        return contenido;
    }

    public void setContenido(Object contenido) {
        this.contenido = contenido;
    }
}
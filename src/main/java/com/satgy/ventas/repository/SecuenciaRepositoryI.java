package com.satgy.ventas.repository;

import com.satgy.ventas.model.Secuencia;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface SecuenciaRepositoryI extends JpaRepository<Secuencia, Integer>{
    
    @Query("Select s from Secuencia s where s.descripcion = :descripcion ")
    public List<Secuencia> buscaPorDescripcion(@Param("descripcion")String descripcion);
}

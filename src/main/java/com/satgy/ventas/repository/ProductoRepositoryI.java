package com.satgy.ventas.repository;

import com.satgy.ventas.model.Producto;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface ProductoRepositoryI extends JpaRepository <Producto, Integer> {
    
    @Query("Select p from Producto p where lower(p.nombre) like lower(concat('%', :nombre, '%'))")
    public Page<Producto> findByNombreLike(@Param("nombre") String nombre, Pageable pageable);
}

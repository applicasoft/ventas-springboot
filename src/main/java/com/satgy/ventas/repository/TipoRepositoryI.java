/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.satgy.ventas.repository;

import com.satgy.ventas.model.Tipo;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author Ronny
 */
public interface TipoRepositoryI extends JpaRepository<Tipo, Integer>{
    
}

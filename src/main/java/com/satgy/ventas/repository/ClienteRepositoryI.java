package com.satgy.ventas.repository;

import com.satgy.ventas.model.Cliente;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ClienteRepositoryI extends JpaRepository<Cliente, Integer>{
    
}

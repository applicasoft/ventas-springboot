package com.satgy.ventas.repository;

import com.satgy.ventas.model.Bitacora;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BitacoraRepositoryI extends JpaRepository <Bitacora, Integer>{
    
}

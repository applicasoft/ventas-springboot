package com.satgy.ventas.repository;

import com.satgy.ventas.model.Archivo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;


public interface ArchivoRepositoryI extends JpaRepository <Archivo, Integer>{
    
    @Query("Select a from Archivo a where a.idarchivo = :idarchivo")
    public Archivo buscaPorId(@Param("idarchivo")Integer idarchivo);
    
    @Query("Select a from Archivo a where a.nombre = :nombre")
    public Archivo buscaPorNombre(@Param("nombre")String nombre);
    
}

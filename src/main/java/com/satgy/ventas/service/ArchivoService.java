package com.satgy.ventas.service;

import com.satgy.ventas.model.Archivo;
import com.satgy.ventas.repository.ArchivoRepositoryI;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ArchivoService implements CrudServiceI<Archivo> {

    // Inyectar dependencias de cliente
    @Autowired
    private ArchivoRepositoryI archivoRepository;
    
    @Override
    public List<Archivo> findAll() {
        return archivoRepository.findAll();
    }

    @Override
    public Optional findById(Integer idarchivo) {
        return archivoRepository.findById(idarchivo);
    }
    
    public Archivo buscaPorId(Integer idarchivo){
        return archivoRepository.buscaPorId(idarchivo);
    }
    
    public Archivo buscaPorNombre(String nombre){
        return archivoRepository.buscaPorNombre(nombre);
    }

    @Override
    public Archivo create(Archivo archivo) {
        return archivoRepository.save(archivo);
    }

    @Override
    public Archivo update(Archivo archivo) {
        return archivoRepository.save(archivo);
    }

    @Override
    public void delete(Integer idarchivo) {
        archivoRepository.deleteById(idarchivo);
    }
    
}
package com.satgy.ventas.service;

import com.satgy.ventas.model.Bitacora;
import com.satgy.ventas.model.Cliente;
import com.satgy.ventas.repository.ClienteRepositoryI;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

// Anotación para poder inyectarla como servicio en el controller
@Service
public class ClienteService implements CrudServiceI<Cliente>{

    // Inyectar dependencias de cliente
    @Autowired
    private ClienteRepositoryI clienteRepository;
    
    @Autowired
    private BitacoraService bitacoraService;
    
    @Override
    public List<Cliente> findAll() {
        bitacoraService.create(new Bitacora("Cliente.findAll"));
        return clienteRepository.findAll();
    }

    @Override
    public Optional findById(Integer idcliente) {
        bitacoraService.create(new Bitacora("Cliente.findById: " + idcliente));
        return clienteRepository.findById(idcliente);
    }

    @Override
    public Cliente create(Cliente cliente) {
        bitacoraService.create(new Bitacora("Cliente.create: " + cliente.getNombreComercial() + " - correo: " + cliente.getCorreo()));
        return clienteRepository.save(cliente);
    }

    @Override
    public Cliente update(Cliente cliente) {
        bitacoraService.create(new Bitacora("Cliente.update: " + cliente.getNombreComercial() + " - correo: " + cliente.getCorreo()));
        return clienteRepository.save(cliente);
    }

    @Override
    public void delete(Integer idcliente) {
        clienteRepository.deleteById(idcliente);
    }
    
}

package com.satgy.ventas.service;

import com.satgy.ventas.model.Persona;
import com.satgy.ventas.repository.PersonaRepositoryI;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

// Anotación para poder inyectarla como servicio en el controller
@Service
public class PersonaService implements CrudServiceI<Persona>{
    
    // Inyectar dependencias para Persona
    @Autowired
    private PersonaRepositoryI personaRepo;

    public List<Persona> findByNombre(String nombre){
        return personaRepo.findByNombre(nombre);
    }
    
    @Override
    public List<Persona> findAll() {
        return personaRepo.findAll();
    }

    @Override
    public Optional findById(Integer personaid) {
        return personaRepo.findById(personaid);
    }

    @Override
    public Persona create(Persona persona) {
        return personaRepo.save(persona);
    }

    @Override
    public Persona update(Persona persona) {
        return personaRepo.save(persona);
    }

    @Override
    public void delete(Integer personaid) {
        personaRepo.deleteById(personaid);
    }
    
}

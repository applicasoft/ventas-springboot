package com.satgy.ventas.service;

import com.satgy.ventas.general.Lista;
import com.satgy.ventas.general.Respuesta;
import com.satgy.ventas.model.Producto;
import com.satgy.ventas.repository.ProductoRepositoryI;
import java.util.List;
import java.util.Optional;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;

@Service
public class ProductoService implements CrudServiceI<Producto>{
    
    @Autowired
    private ProductoRepositoryI productoRepositorio;
    
    public Respuesta getAllPageable(Integer pageNo, Integer pageSize, String sortBy)
    {
        Pageable paging = PageRequest.of(pageNo, pageSize, Sort.by(sortBy).descending());
        Page<Producto> pagedResult = productoRepositorio.findAll(paging);
        return Respuesta.crearDePaged(pagedResult, pageSize, pageNo);
    }
    
    public Respuesta getNombrePageable(String nombre, Integer pageNo, Integer pageSize, String sortBy)
    {
        Pageable paging = PageRequest.of(pageNo, pageSize, Sort.by(sortBy));
        Page<Producto> pagedResult = productoRepositorio.findByNombreLike(nombre, paging);
        return Respuesta.crearDePaged(pagedResult, pageSize, pageNo);
    }
    
    @PersistenceContext
    private EntityManager em;
    
    public List<Producto> findEM(){
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Producto> cq = cb.createQuery(Producto.class);
        Root<Producto> fromProducto = cq.from(Producto.class);
        cq.select(fromProducto);
        //cq.where(cb.equal(fromProducto.get("nombre"), "Calculadora")); 
        TypedQuery<Producto> query = em.createQuery(cq);
        return query.getResultList();
    }
    
    
    public Respuesta findParametrosPageable(Lista <String, String> parametros, Integer pageNo, Integer pageSize){
        
        String buscaNombre = "", buscaPrecio = "";
        String jpql = "select p from Producto p where p.idproducto > 0 ";
        //String jpql = "select p from Producto p where lower(p.nombre) like '%" + parametros.getValue("nombre") + "%' ";
        
        if (parametros.existe("nombre")) {
            buscaNombre = " and (lower(p.nombre) like '%" + parametros.getValue("nombre").toLowerCase() + "%' or "
                    + " lower(p.codigo) like '%" + parametros.getValue("nombre").toLowerCase() + "%' )";
        }
        if (parametros.existe("precio minimo") && parametros.existe("precio maximo") ) {
            buscaPrecio = " and p.precio between " + parametros.getValue("precio minimo") + " and " + parametros.getValue("precio maximo") + " ";
        } else if (parametros.existe("precio minimo")) { 
            buscaPrecio = " and p.precio >= " + parametros.getValue("precio minimo") + " ";
        } else if (parametros.existe("precio maximo")) { 
            buscaPrecio = " and p.precio <= " + parametros.getValue("precio maximo") + " ";
        }
        
        Query q = em.createQuery(jpql + buscaNombre + buscaPrecio + " order by p.codigo desc");
        return Respuesta.crearDeQuery(pageSize, pageNo, q);
        
    }
    
    public List<Producto> findParametros(Lista <String, String> parametros){
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Producto> cq = cb.createQuery(Producto.class);
        Root<Producto> fromProducto = cq.from(Producto.class);
        cq.select(fromProducto);
        TypedQuery<Producto> query = em.createQuery(cq);

        String buscaNombre = "", buscaPrecio = "";
        String jpql = "select p from Producto p where p.idproducto > 0 ";
        //String jpql = "select p from Producto p where lower(p.nombre) like '%" + parametros.getValue("nombre") + "%' ";
        
        if (parametros.existe("nombre")) {
            cq.where(cb.like(fromProducto.get("nombre"), parametros.getValue("nombre"))); 
            buscaNombre = " and (lower(p.nombre) like '%" + parametros.getValue("nombre").toLowerCase() + "%' or "
                    + " lower(p.codigo) like '%" + parametros.getValue("nombre").toLowerCase() + "%' )";
        }
        
        if (parametros.existe("precio minimo") && parametros.existe("precio maximo") ) {
            Double precioMin = Double.parseDouble(parametros.getValue("precio minimo"));
            Double precioMax = Double.parseDouble(parametros.getValue("precio maximo"));
            cq.where(cb.between(fromProducto.get("precio"), precioMin, precioMax)); 
            buscaPrecio = " and p.precio between " + parametros.getValue("precio minimo") + " and " + parametros.getValue("precio maximo") + " ";
        } else if (parametros.existe("precio minimo")) { 
            Double precioMin = Double.parseDouble(parametros.getValue("precio minimo"));
            cq.where(cb.greaterThanOrEqualTo(fromProducto.get("precio"), precioMin));
            buscaPrecio = " and p.precio >= " + parametros.getValue("precio minimo") + " ";
        } else if (parametros.existe("precio maximo")) { 
            Double precioMax = Double.parseDouble(parametros.getValue("precio maximo"));
            cq.where(cb.lessThanOrEqualTo(fromProducto.get("precio"), precioMax));
            buscaPrecio = " and p.precio <= " + parametros.getValue("precio maximo") + " ";
        }
        
        Query q = em.createQuery(jpql + buscaNombre + buscaPrecio + " order by p.codigo desc");
        return q.getResultList();
        
        //return query.getResultList();
    }
    
    @Override
    public List<Producto> findAll() {
        return productoRepositorio.findAll();
    }

    @Override
    public Optional findById(Integer idproducto) {
        return productoRepositorio.findById(idproducto);
    }

    @Override
    public Producto create(Producto producto) {
        return productoRepositorio.save(producto);
    }

    @Override
    public Producto update(Producto producto) {
        return productoRepositorio.save(producto);
    }

    @Override
    public void delete(Integer idproducto) {
        productoRepositorio.deleteById(idproducto);
    }
}

package com.satgy.ventas.service;

import com.satgy.ventas.model.Tipo;
import com.satgy.ventas.repository.TipoRepositoryI;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TipoService implements CrudServiceI<Tipo> {

    @Autowired
    private TipoRepositoryI tipoRepository;
    
    @Override
    public List<Tipo> findAll() {
        return tipoRepository.findAll();
    }

    @Override
    public Optional findById(Integer idtipo) {
        return tipoRepository.findById(idtipo);
    }

    @Override
    public Tipo create(Tipo tipo) {
        return tipoRepository.save(tipo);
    }

    @Override
    public Tipo update(Tipo tipo) {
        return tipoRepository.save(tipo);
    }

    @Override
    public void delete(Integer idtipo) {
        tipoRepository.deleteById(idtipo);
    }
    
}
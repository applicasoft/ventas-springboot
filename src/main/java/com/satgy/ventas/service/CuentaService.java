package com.satgy.ventas.service;

import com.satgy.ventas.model.Cuenta;
import com.satgy.ventas.repository.CuentaRepositoryI;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CuentaService implements CrudServiceI<Cuenta> {

    @Autowired
    private CuentaRepositoryI cuentaRepository;
    
    @Override
    public List<Cuenta> findAll() {
        return cuentaRepository.findAll();
    }

    @Override
    public Optional findById(Integer idcuenta) {
        return cuentaRepository.findById(idcuenta);
    }

    @Override
    public Cuenta create(Cuenta cuenta) {
        return cuentaRepository.save(cuenta);
    }

    @Override
    public Cuenta update(Cuenta cuenta) {
        return cuentaRepository.save(cuenta);
    }

    @Override
    public void delete(Integer idcuenta) {
        cuentaRepository.deleteById(idcuenta);
    }
    
}
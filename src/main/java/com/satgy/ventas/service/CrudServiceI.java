package com.satgy.ventas.service;

import java.util.List;
import java.util.Optional;

// T aceptará como parámetro a una clase, para que se puedan usar estos métodos
public interface CrudServiceI <T> {
    public List<T> findAll();
    public Optional findById(Integer id);
    public T create(T modelo);
    public T update(T modelo);
    public void delete(Integer id);
}

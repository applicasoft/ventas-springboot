package com.satgy.ventas.service;

import com.satgy.ventas.model.Bitacora;
import com.satgy.ventas.repository.BitacoraRepositoryI;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BitacoraService implements CrudServiceI<Bitacora> {

    // Inyectar dependencias de cliente
    @Autowired
    private BitacoraRepositoryI bitacoraRepository;
    
    @Override
    public List<Bitacora> findAll() {
        return bitacoraRepository.findAll();
    }

    @Override
    public Optional findById(Integer idbitacora) {
        return bitacoraRepository.findById(idbitacora);
    }

    @Override
    public Bitacora create(Bitacora bitacora) {
        return bitacoraRepository.save(bitacora);
    }

    @Override
    public Bitacora update(Bitacora bitacora) {
        return bitacoraRepository.save(bitacora);
    }

    @Override
    public void delete(Integer idbitacora) {
        bitacoraRepository.deleteById(idbitacora);
    }
    
}
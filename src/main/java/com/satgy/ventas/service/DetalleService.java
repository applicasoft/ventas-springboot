package com.satgy.ventas.service;

import com.satgy.ventas.model.Detalle;
import com.satgy.ventas.repository.DetalleRepositoryI;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DetalleService implements CrudServiceI<Detalle>{

    @Autowired
    private DetalleRepositoryI detalleRepository;
    
    @Override
    public List<Detalle> findAll() {
        return detalleRepository.findAll();
    }

    @Override
    public Optional findById(Integer iddetalle) {
        return detalleRepository.findById(iddetalle);
    }

    @Override
    public Detalle create(Detalle venta) {
        return detalleRepository.save(venta);
    }

    @Override
    public Detalle update(Detalle venta) {
        return detalleRepository.save(venta);
    }

    @Override
    public void delete(Integer iddetalle) {
        detalleRepository.deleteById(iddetalle);
    }

}

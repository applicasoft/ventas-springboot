package com.satgy.ventas.service;

import com.satgy.ventas.dto.VentaDto;
import com.satgy.ventas.model.Detalle;
import com.satgy.ventas.model.Venta;
import com.satgy.ventas.repository.DetalleRepositoryI;
import com.satgy.ventas.repository.VentaRepositoryI;
import java.util.List;
import java.util.Optional;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class VentaService implements CrudServiceI<Venta>{

    @Autowired
    private VentaRepositoryI ventaRepository;
    
    @Autowired
    private DetalleRepositoryI detalleRepository;
    
    @Autowired
    private ModelMapper modelMapper;
    
    @Override
    public List<Venta> findAll() {
        return ventaRepository.findAll();
    }

    @Override
    public Optional findById(Integer idventa) {
        return ventaRepository.findById(idventa);
    }

    @Override
    public Venta create(Venta venta) {
        //detalleRepository.saveAll(venta.getDetalles()); // no funcionó
        VentaDto ventaDto = modelMapper.map(venta, VentaDto.class);
        venta.eliminaHijos();
        Venta v2 = ventaRepository.save(venta);
        
        for (Detalle d : ventaDto.getDetalles()){
            d.setVenta(venta);
            detalleRepository.save(d);
        }
        v2.setDetalles(ventaDto.getDetalles());
        return v2;
    }
    
    @Override
    public Venta update(Venta venta) {
        return ventaRepository.save(venta);
    }

    @Override
    public void delete(Integer idventa) {
        ventaRepository.deleteById(idventa);
    }

}

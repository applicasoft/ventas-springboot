package com.satgy.ventas.service;

import com.satgy.ventas.general.Cadenas;
import com.satgy.ventas.model.Secuencia;
import com.satgy.ventas.repository.SecuenciaRepositoryI;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SecuenciaService implements CrudServiceI<Secuencia> {

    @Autowired
    private SecuenciaRepositoryI secuenciaRepository;

    private Secuencia getOrInsertSecuencia(String strSecuencia){
        // Obtiene o crea la secuencia, strSecuencia es así: 'nombreArchivo.15'
        String descripcion = strSecuencia.split("\\.")[0];
        Integer ceros = Integer.parseInt(strSecuencia.split("\\.")[1]);
        List<Secuencia> secuencias = secuenciaRepository.buscaPorDescripcion(descripcion);
        if (secuencias.size() == 0){
            // Si no existe la secuencia entonces crearla
            Secuencia secuencia = new Secuencia(1, descripcion, ceros);
            secuenciaRepository.save(secuencia);
            return secuencia;
        } else {
            return secuencias.get(0);
        }
    }
    
    public Integer getValorInt(String strSecuencia){
        return getOrInsertSecuencia(strSecuencia).getValor();
    }    
    public String getValorStr(String strSecuencia){
        Secuencia secuencia = getOrInsertSecuencia(strSecuencia);
        return Cadenas.ceros(secuencia.getValor(), secuencia.getCeros());
    }
    
    public void aumentar(String strSecuencia){
        Secuencia secuencia = getOrInsertSecuencia(strSecuencia);
        secuencia.setValor(secuencia.getValor() + 1);
        update(secuencia);
    }
    
    @Override
    public List<Secuencia> findAll() {
        return secuenciaRepository.findAll();
    }

    @Override
    public Optional findById(Integer idsecuencia) {
        return secuenciaRepository.findById(idsecuencia);
    }

    @Override
    public Secuencia create(Secuencia secuencia) {
        return secuenciaRepository.save(secuencia);
    }

    @Override
    public Secuencia update(Secuencia secuencia) {
        return secuenciaRepository.save(secuencia);
    }

    @Override
    public void delete(Integer idsecuencia) {
        secuenciaRepository.deleteById(idsecuencia);
    }
    
}
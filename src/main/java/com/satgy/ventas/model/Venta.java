package com.satgy.ventas.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import javax.persistence.*;
import javax.validation.constraints.*;

@Entity
@Table(name = "venta")
public class Venta implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idventa;
    
    @NotNull(message = "Ingresa la fecha")
    //@NotBlank(message = "Ingresa la fecha")
    @Column(name = "fecha", nullable = false)
    private Date fecha;
    
    @ManyToOne
    @JoinColumn(name = "idcliente", nullable = false, referencedColumnName = "idcliente")
    private Cliente cliente;
    
    // mappedBy = venta, venta es la variable en Detalle para enlazar con @JoinColumn a la tabla venta
    @OneToMany(mappedBy = "venta", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<Detalle> detalles;

    public Integer getIdventa() {
        return idventa;
    }

    public void setIdventa(Integer idventa) {
        this.idventa = idventa;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public List<Detalle> getDetalles() {
        return detalles;
    }

    public void setDetalles(List<Detalle> detalles) {
        this.detalles = detalles;
    }

    public void eliminaHijos(){
        if (detalles != null){ this.detalles.clear(); }
    }
    
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 23 * hash + Objects.hashCode(this.idventa);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Venta other = (Venta) obj;
        if (!Objects.equals(this.idventa, other.idventa)) {
            return false;
        }
        return true;
    }
    
    
}

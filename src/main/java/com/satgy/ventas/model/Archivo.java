package com.satgy.ventas.model;

import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.*;

@Entity
@Table(name="archivo")
public class Archivo implements Serializable{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idarchivo;
    
    @NotNull(message = "Por favor ingresa el nombre")
    @Size(min = 3, max = 50, message = "El nombre debe tener de 3 a 50 caracteres")
    @Column(name = "nombre", nullable = false, length = 50)
    private String nombre;
    
    //@NotNull(message = "Por favor ingresa el nombre")
    @Size(min = 3, max = 500, message = "El nombre original debe tener de 3 a 50 caracteres")
    @Column(name = "nombreoriginal", nullable = true, length = 500)
    private String nombreOriginal;
    
    @NotNull(message = "Por favor ingresa el uri descarga")
    @Size(min = 3, max = 500, message = "El uri de descarga debe tener de 3 a 500 caracteres")
    @Column(name = "uridescarga", nullable = false, length = 500)
    private String uriDescarga;
    
    //@NotNull(message = "Por favor ingresa el tipo")
    @Size(min = 1, max = 100, message = "El tipo debe tener de 1 a 100 caracteres")
    @Column(name = "tipo", nullable = true, length = 100)
    private String tipo;
    
    @Column(name = "tamano", nullable = true)
    private long tamano;

    public Archivo() {
    }

    public Archivo(String nombre, String nombreOriginal, String uriDescarga, String tipo, long tamano) {
        this.nombre = nombre;
        this.nombreOriginal = nombreOriginal;
        this.uriDescarga = uriDescarga;
        this.tipo = tipo;
        this.tamano = tamano;
    }

    public String getNombreOriginal() {
        return nombreOriginal;
    }

    public void setNombreOriginal(String nombreOriginal) {
        this.nombreOriginal = nombreOriginal;
    }
    
    public Integer getIdarchivo() {
        return idarchivo;
    }

    public void setIdarchivo(Integer idarchivo) {
        this.idarchivo = idarchivo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getUriDescarga() {
        return org.springframework.web.servlet.support.ServletUriComponentsBuilder.fromCurrentContextPath()
                .path("/api/archivo/downloadFile/")
                .path(nombre)
                .toUriString();
        //return uriDescarga;
    }

    public void setUriDescarga(String uriDescarga) {
        this.uriDescarga = uriDescarga;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public long getTamano() {
        return tamano;
    }

    public void setTamano(long tamano) {
        this.tamano = tamano;
    }
}
package com.satgy.ventas.model;

import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.*;

@Entity
@Table(name = "cuenta")
public class Cuenta implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idcuenta;
    
    @NotNull(message = "Por favor ingresa el nombre")
    @NotBlank(message = "Ingresa el nombre")
    @Size(min = 3, max = 50, message = "El nombre debe tener de 3 a 50 caracteres")
    @Column(name = "nombre", nullable = false, length = 50)
    private String nombre;

    @OneToOne
    @JoinColumn(name = "idcuentapadre", nullable = true, referencedColumnName = "idcuenta")
    private Cuenta padre;

    public Integer getIdcuenta() {
        return idcuenta;
    }

    public void setIdcuenta(Integer idcuenta) {
        this.idcuenta = idcuenta;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Cuenta getPadre() {
        return padre;
    }

    public void setPadre(Cuenta padre) {
        this.padre = padre;
    }
    
    
}
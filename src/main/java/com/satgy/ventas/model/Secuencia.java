package com.satgy.ventas.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;

@Entity
@Table(name="secuencia")
public class Secuencia implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idsecuencia;
    
    @Column(name = "valor", nullable = false)
    private Integer valor;
    
    @Column(name = "descripcion", nullable = false, length = 100)
    private String descripcion;

    @Column(name = "ceros", nullable = false)
    private Integer ceros;

    public Secuencia() {
    }

    /**
     * Me permite hacer el insert
     * @param valor
     * @param descripcion
     * @param ceros 
     */
    public Secuencia(Integer valor, String descripcion, Integer ceros) {
        this.valor = valor;
        this.descripcion = descripcion;
        this.ceros = ceros;
    }
    
    public Integer getIdsecuencia() {
        return idsecuencia;
    }

    public void setIdsecuencia(Integer idsecuencia) {
        this.idsecuencia = idsecuencia;
    }

    public Integer getValor() {
        return valor;
    }

    public void setValor(Integer valor) {
        this.valor = valor;
    }

    
    
    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Integer getCeros() {
        return ceros;
    }

    public void setCeros(Integer ceros) {
        this.ceros = ceros;
    }

    
}

package com.satgy.ventas.model;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.*;
import javax.validation.constraints.*;

@Entity
@Table(name = "ventadetalle")
public class Detalle implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer iddetalle;
    
    @ManyToOne
    @JoinColumn(name = "idventa", nullable = false, referencedColumnName = "idventa")
    private Venta venta;
    
    @ManyToOne
    @JoinColumn(name = "idproducto", nullable = false, referencedColumnName = "idproducto")
    private Producto producto;
    
    @NotNull(message = "Por favor ingresa el precio unitario")
    //@NotBlank(message = "Ingresa el precio unitario")
    @Column(name = "preciounitario", nullable = false)
    private Double precioUnitario;
    
    @NotNull(message = "Por favor ingresa la cantidad")
    //@NotBlank(message = "Ingresa la cantidad")
    @Column(name = "cantidad", nullable = false)
    private Integer cantidad;

    public Detalle eliminaPadre(){
        venta = null;
        return this;
    }
    
    public Integer getIddetalle() {
        return iddetalle;
    }

    public void setIddetalle(Integer iddetalle) {
        this.iddetalle = iddetalle;
    }

    public Venta getVenta() {
        return venta;
    }

    public void setVenta(Venta venta) {
        this.venta = venta;
    }
    
    public void delVenta(){
        venta = null;
    }

    public Producto getProducto() {
        return producto;
    }

    public void setProducto(Producto producto) {
        this.producto = producto;
    }

    public Double getPrecioUnitario() {
        return precioUnitario;
    }

    public void setPrecioUnitario(Double precioUnitario) {
        this.precioUnitario = precioUnitario;
    }

    public Integer getCantidad() {
        return cantidad;
    }

    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 29 * hash + Objects.hashCode(this.iddetalle);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Detalle other = (Detalle) obj;
        if (!Objects.equals(this.iddetalle, other.iddetalle)) {
            return false;
        }
        return true;
    }
    
    
}

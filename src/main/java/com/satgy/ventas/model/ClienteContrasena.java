package com.satgy.ventas.model;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.*;
import javax.validation.constraints.*;

@Entity
@Table(name="clientecontrasena")
public class ClienteContrasena implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idclientecontrasena;
    
    @Column(name = "contrasena", nullable = false, length = 50)
    private String contrasena;
    
    @OneToOne
    @JoinColumn(name = "idcliente", nullable = false, referencedColumnName = "idcliente")
    private Cliente cliente;
    
    @Override
    public int hashCode() {
        int hash = 3;
        hash = 83 * hash + Objects.hashCode(this.idclientecontrasena);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ClienteContrasena other = (ClienteContrasena) obj;
        if (!Objects.equals(this.idclientecontrasena, other.idclientecontrasena)) {
            return false;
        }
        return true;
    }

    
}

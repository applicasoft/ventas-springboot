package com.satgy.ventas.model;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.*;
import javax.validation.constraints.*;

@Entity
@Table(name = "Producto")
public class Producto implements Serializable {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idproducto;
    
    @NotNull(message = "Por favor ingresa el código")
    @NotBlank(message = "Ingresa el código")
    @Size(min = 1, max = 20, message = "El código debe tener de 1 a 20 caracteres")
    @Column(name = "codigo", nullable = false, length = 20)
    private String codigo;
    
    @NotNull(message = "Por favor ingresa el nombre")
    @NotBlank(message = "Ingresa el nombre")
    @Size(min = 1, max = 20, message = "El nombre debe tener de 3 a 20 caracteres")
    @Column(name = "nombre", nullable = false, length = 20)
    private String nombre;
    
    @NotNull(message = "Por favor ingresa el precio")
    // @NotBlank(message = "Ingresa el precio") // no debo poner esta porque no acepta valores númericos.
    @Column(name = "precio", nullable = false)
    private Double precio;

    @ManyToOne
    @JoinColumn(name = "idtipo", nullable = false, referencedColumnName = "idtipo")
    private Tipo tipo;
    
    @ManyToOne
    @JoinColumn(name = "idarchivoimagen", nullable = true, referencedColumnName = "idarchivo")
    private Archivo archivoImagen;
    
    public Integer getIdproducto() {
        return idproducto;
    }

    public void setIdproducto(Integer idproducto) {
        this.idproducto = idproducto;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Double getPrecio() {
        return precio;
    }

    public void setPrecio(Double precio) {
        this.precio = precio;
    }

    public Tipo getTipo() {
        return tipo;
    }

    public void setTipo(Tipo tipo) {
        this.tipo = tipo;
    }
    
    @Override
    public int hashCode() {
        int hash = 5;
        hash = 41 * hash + Objects.hashCode(this.idproducto);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Producto other = (Producto) obj;
        if (!Objects.equals(this.idproducto, other.idproducto)) {
            return false;
        }
        return true;
    }

    public Archivo getArchivoImagen() {
        return archivoImagen;
    }

    public void setArchivoImagen(Archivo archivoImagen) {
        this.archivoImagen = archivoImagen;
    }

}

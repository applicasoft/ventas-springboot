package com.satgy.ventas.model;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.*;
import javax.validation.constraints.*;

@Entity
@Table(name="cliente")
public class Cliente implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idcliente;
    
    @NotNull(message = "Ingresa la identificación")
    @NotBlank(message = "Por favor ingresa la identificación")
    @Size(min = 3, message = "La identificación debe tener al menos 3 caracteres")
    @Pattern(regexp = "^[0-9]*$")
    @Column(name = "identificacion", nullable = false, length = 20)
    private String identificacion;
    
    @Column(name = "nombrecomercial", nullable = true, length = 50)
    private String nombreComercial;
    
    @OneToOne
    @JoinColumn(name = "idpersona", nullable = false, referencedColumnName = "idpersona")
    private Persona persona;
    
    @NotNull
    @NotBlank(message = "Por favor ingresa el correo")
    @Size(min = 3, message = "El correo debe tener al menos 3 caracteres")
    @Email(message = "Ingresa un correo valido")
    @Column(name = "correo", nullable = false, length = 50)
    private String correo;

    public Integer getIdcliente() {
        return idcliente;
    }

    public void setIdcliente(Integer idcliente) {
        this.idcliente = idcliente;
    }

    public String getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getNombreComercial() {
        return nombreComercial;
    }

    public void setNombreComercial(String nombreComercial) {
        this.nombreComercial = nombreComercial;
    }

    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 83 * hash + Objects.hashCode(this.idcliente);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Cliente other = (Cliente) obj;
        if (!Objects.equals(this.idcliente, other.idcliente)) {
            return false;
        }
        return true;
    }

    
}

package com.satgy.ventas.model;

import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.*;

@Entity
@Table(name="persona")
public class Persona implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idpersona;
    
    @NotNull(message = "Por favor ingresa el nombre")
    @NotBlank(message = "Ingresa el nombre")
    @Size(min = 3, max = 70, message = "El nombre debe tener de 3 a 70 caracteres")
    @Column(name = "nombre", nullable = false, length = 70)
    private String nombre;
    
    @Size(max = 250, message = "La dirección no debe tener más de 250 caracteres")
    @Column(name = "direccion", nullable = true, length = 250)
    private String direccion;
    
    @Size(min = 6, max = 20, message = "El teléfono debe tener de 6 a 20 caracteres")
    @Column(name = "telefono", nullable = true, length = 20)
    private String telefono;

    public Integer getIdpersona() {
        return idpersona;
    }

    public void setIdpersona(Integer idpersona) {
        this.idpersona = idpersona;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }
    
}

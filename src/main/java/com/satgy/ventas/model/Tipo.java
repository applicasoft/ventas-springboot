package com.satgy.ventas.model;

import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.*;

@Entity
@Table(name = "productotipo")
public class Tipo implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idtipo;
    
    @NotNull(message = "Por favor ingresa el código")
    @NotBlank(message = "Ingresa el código")
    @Size(min = 1, max = 20, message = "El código debe tener de 1 a 20 caracteres")
    @Column(name = "codigo", nullable = false, length = 20)
    private String codigo;
    
    @NotNull(message = "Por favor ingresa el nombre")
    @NotBlank(message = "Ingresa el nombre")
    @Size(min = 3, max = 50, message = "El nombre debe tener de 3 a 50 caracteres")
    @Column(name = "nombre", nullable = false, length = 50)
    private String nombre;

    public Integer getIdtipo() {
        return idtipo;
    }

    public void setIdtipo(Integer idtipo) {
        this.idtipo = idtipo;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }   
}
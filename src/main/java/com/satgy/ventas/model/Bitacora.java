package com.satgy.ventas.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.*;

@Entity
@Table(name="bitacora")
public class Bitacora implements Serializable {
    
    public Bitacora() {}
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idbitacora;
    
    @NotNull(message = "Por favor ingresa la descripción")
    @NotBlank(message = "Ingresa la descripción")
    @Size(min = 3, max = 3000, message = "La descripción debe tener de 3 a 3000 caracteres")
    @Column(name = "descripcion", nullable = false, length = 3000)
    private String descripcion;
    
    @Column(name = "fecha", nullable = false)
    private Date fecha;

    public Bitacora(String descripcion) {
        this.descripcion = descripcion;
        this.fecha = new java.util.Date();
    }
    
    public Integer getIdbitacora() {
        return idbitacora;
    }

    public void setIdbitacora(Integer idbitacora) {
        this.idbitacora = idbitacora;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }
    
    
}

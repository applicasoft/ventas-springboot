package com.satgy.ventas.config;

import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = { "com.satgy.ventas.model", "com.satgy.ventas.dto",
        "com.satgy.ventas.service", "com.satgy.ventas.repository", "com.satgy.ventas.controller" })
public class ApplicationConfiguration {

    @Bean
    public ModelMapper modelMapper() {
        return new ModelMapper();
    }
}

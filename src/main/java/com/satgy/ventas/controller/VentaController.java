package com.satgy.ventas.controller;

import com.satgy.ventas.dto.VentaDto;
import com.satgy.ventas.model.Venta;
import com.satgy.ventas.service.VentaService;
import java.util.List;
import java.util.stream.Collectors;
import javax.validation.Valid;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/venta")
public class VentaController {
    
    @Autowired
    private VentaService ventaService;
    
    @Autowired
    private ModelMapper modelMapper;
    
    <S, T> List<T> mapList(List<S> source, Class<T> targetClass) {
    return source
        .stream()
        .map(element -> modelMapper.map(element, targetClass))
        .collect(Collectors.toList());
    }
    
    @GetMapping
    public ResponseEntity<List<VentaDto>> findAll(){
        List <VentaDto> dtos = mapList(ventaService.findAll(), VentaDto.class);
        for (VentaDto dto : dtos) { 
            dto.eliminaHijos(); // solo me interesa tener la cabecera por ahora
        }
        return ResponseEntity.ok(dtos);
    }
    
    @GetMapping("/{id}")
    public ResponseEntity<VentaDto> findById(@PathVariable("id") Integer idventa){
           return (ResponseEntity<VentaDto>) ventaService.findById(idventa)
                .map(ven ->{
                        VentaDto dto = modelMapper.map(ven, VentaDto.class);
                        dto.eliminaPadreDeHijos();
                        return ResponseEntity.ok(dto);
                        })
                
                // esto de abajo con .map va en el caso de que en del detalle muestre la venta
                // porque cuando lo quiero ver en el postman, me genera recursividad en los datos
                // debido a que la venta contiene el detalle, y el detalle la venta
                // lo mejor es que en el detalle no me muestre los datos de la venta
                /*.map(venta -> {
                    Venta v = (Venta) venta;
                    v.delDetalles();
                    return  ResponseEntity.ok(v);
                })*/
                .orElseGet(()->ResponseEntity.notFound().build());
    }
    
    @PostMapping
    public ResponseEntity<VentaDto> create(@Valid @RequestBody Venta venta){
        VentaDto dto = modelMapper.map(ventaService.create(venta), VentaDto.class);
        dto.eliminaPadreDeHijos();
        return new ResponseEntity<>(dto, HttpStatus.CREATED);
    }
    
    @PutMapping
    public ResponseEntity<VentaDto> update(@Valid @RequestBody Venta venta){
        return (ResponseEntity<VentaDto>) ventaService.findById(venta.getIdventa())
                .map(ven -> {
                    VentaDto dto = modelMapper.map(ventaService.update(venta), VentaDto.class);
                    dto.eliminaPadreDeHijos();
                    return ResponseEntity.ok(dto);
                })
                .orElseGet(()->ResponseEntity.notFound().build());
    }
    
    @DeleteMapping("/{id}")
    public ResponseEntity<VentaDto> delete (@PathVariable("id") Integer idventa){
        return (ResponseEntity<VentaDto>) ventaService.findById(idventa)
                .map(venta -> {
                    ventaService.delete(idventa);
                    return ResponseEntity.ok(modelMapper.map(venta, VentaDto.class).eliminaPadreDeHijos());
                })
                .orElseGet(()->ResponseEntity.notFound().build());
    }
}

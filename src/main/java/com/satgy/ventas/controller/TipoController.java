package com.satgy.ventas.controller;

import com.satgy.ventas.model.Tipo;
import com.satgy.ventas.service.TipoService;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/tipo")
public class TipoController {
    
    @Autowired
    private TipoService tipoService;
    
    @GetMapping
    public ResponseEntity<List<Tipo>> findAll(){
        return ResponseEntity.ok(tipoService.findAll());
    }
    
    @GetMapping("/{id}")
    public ResponseEntity<Tipo> findById(@PathVariable("id") Integer idtipo){
        return (ResponseEntity<Tipo>) tipoService.findById(idtipo)
                .map(ResponseEntity::ok)
                .orElseGet(()->ResponseEntity.notFound().build());
    }
    
    @PostMapping
    public ResponseEntity<Tipo> create(@Valid @RequestBody Tipo tipo){
        return new ResponseEntity<>(tipoService.create(tipo), HttpStatus.CREATED);
    }
    
    @PutMapping
    public ResponseEntity<Tipo> update(@Valid @RequestBody Tipo tipo){
        return (ResponseEntity<Tipo>) tipoService.findById(tipo.getIdtipo())
                .map(t -> ResponseEntity.ok(tipoService.update(tipo)))
                .orElseGet(()->ResponseEntity.notFound().build());
    }
    
    @DeleteMapping("/{id}")
    public ResponseEntity<Tipo> delete (@PathVariable("id") Integer idtipo){
        return (ResponseEntity<Tipo>) tipoService.findById(idtipo)
                .map(t -> {
                    tipoService.delete(idtipo);
                    return ResponseEntity.ok(t);
                })
                .orElseGet(()->ResponseEntity.notFound().build());
    }
}

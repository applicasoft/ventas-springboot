package com.satgy.ventas.controller;

import com.satgy.ventas.model.Detalle;
import com.satgy.ventas.service.DetalleService;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/detalle")
public class DetalleController {
    
    @Autowired
    private DetalleService detalleService;
    
    @GetMapping
    public ResponseEntity<List<Detalle>> findAll(){
        return ResponseEntity.ok(detalleService.findAll());
    }
    
    @GetMapping("/{id}")
    public ResponseEntity<Detalle> findById(@PathVariable("id") Integer iddetalle){
        return (ResponseEntity<Detalle>) detalleService.findById(iddetalle)
                .map(ResponseEntity::ok)
                .orElseGet(()->ResponseEntity.notFound().build());
    }
    
    @PostMapping
    public ResponseEntity<Detalle> create(@Valid @RequestBody Detalle detalle){
        return new ResponseEntity<>(detalleService.create(detalle), HttpStatus.CREATED);
    }
    
    @PutMapping
    public ResponseEntity<Detalle> update(@Valid @RequestBody Detalle detalle){
        return (ResponseEntity<Detalle>) detalleService.findById(detalle.getIddetalle())
                .map(det -> ResponseEntity.ok(detalleService.update(detalle)))
                .orElseGet(()->ResponseEntity.notFound().build());
    }
    
    @DeleteMapping("/{id}")
    public ResponseEntity<Detalle> delete (@PathVariable("id") Integer iddetalle){
        return (ResponseEntity<Detalle>) detalleService.findById(iddetalle)
                .map(det -> {
                    detalleService.delete(iddetalle);
                    return ResponseEntity.ok(det);
                })
                .orElseGet(()->ResponseEntity.notFound().build());
    }
}

package com.satgy.ventas.controller;

import com.satgy.ventas.model.Cuenta;
import com.satgy.ventas.service.CuentaService;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/cuenta")
public class CuentaController {
    
    @Autowired
    private CuentaService cuentaService;
    
    @GetMapping
    public ResponseEntity<List<Cuenta>> findAll(){
        return ResponseEntity.ok(cuentaService.findAll());
    }
    
    @GetMapping("/{id}")
    public ResponseEntity<Cuenta> findById(@PathVariable("id") Integer idcuenta){
        return (ResponseEntity<Cuenta>) cuentaService.findById(idcuenta)
                .map(ResponseEntity::ok)
                .orElseGet(()->ResponseEntity.notFound().build());
    }
    
    @PostMapping
    public ResponseEntity<Cuenta> create(@Valid @RequestBody Cuenta cuenta){
        return new ResponseEntity<>(cuentaService.create(cuenta), HttpStatus.CREATED);
    }
    
    @PutMapping
    public ResponseEntity<Cuenta> update(@Valid @RequestBody Cuenta cuenta){
        return (ResponseEntity<Cuenta>) cuentaService.findById(cuenta.getIdcuenta())
                .map(t -> ResponseEntity.ok(cuentaService.update(cuenta)))
                .orElseGet(()->ResponseEntity.notFound().build());
    }
    
    @DeleteMapping("/{id}")
    public ResponseEntity<Cuenta> delete (@PathVariable("id") Integer idcuenta){
        return (ResponseEntity<Cuenta>) cuentaService.findById(idcuenta)
                .map(t -> {
                    cuentaService.delete(idcuenta);
                    return ResponseEntity.ok(t);
                })
                .orElseGet(()->ResponseEntity.notFound().build());
    }
}

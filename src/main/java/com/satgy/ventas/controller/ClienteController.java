package com.satgy.ventas.controller;

import com.satgy.ventas.dto.ClienteDto;
import com.satgy.ventas.model.Cliente;
import com.satgy.ventas.service.ClienteService;
import java.util.List;
import java.util.stream.Collectors;
import javax.validation.Valid;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

// request mapping contiene el path o endpoint de los métodos WS, queda así: http://localhost:6060/api/cliente/

@RestController
@RequestMapping("/api/cliente")
public class ClienteController {
    
    @Autowired
    private ClienteService clienteService;
    
    @Autowired
    private ModelMapper modelMapper;
    
    <S, T> List<T> mapList(List<S> source, Class<T> targetClass) {
    return source
        .stream()
        .map(element -> modelMapper.map(element, targetClass))
        .collect(Collectors.toList());
    }
    
    @GetMapping
    public ResponseEntity<List<ClienteDto>> findAll(){
        //int a=23; // hago esto para saber que puedo usar esta forma convencional de programar y enviar varios return. Porque luego uso .map.orElseGet lambda(->), etc.
        //if (a==23) return ResponseEntity.ok(clienteService.findAll()); // retorna el método ok, que es el código 200
        
        List<Cliente> clientes = clienteService.findAll();
        
        // forma 1
        /*List<ClienteDto> dtos = new ArrayList();
        for (Cliente c : clientes){
            ClienteDto clienteDto = modelMapper.map(c, ClienteDto.class);    
            dtos.add(clienteDto);
        }*/

        // forma 2
        /*List<ClienteDto> dtos = clientes
                .stream()
                .map(cli -> modelMapper.map(cli, ClienteDto.class))
                .collect(Collectors.toList());*/
        // forma 3
        List<ClienteDto> dtos = mapList(clientes, ClienteDto.class);
        return ResponseEntity.ok(dtos);

        
    }
    
        
    @GetMapping("/{id}")
    public ResponseEntity<ClienteDto> findById(@PathVariable("id") Integer idcliente){
        
        return (ResponseEntity<ClienteDto>) clienteService.findById(idcliente)
                .map(cli -> {
                            ClienteDto clienteDto = modelMapper.map(cli, ClienteDto.class);
                            return ResponseEntity.ok(clienteDto);
                        })
                .orElseGet(() -> ResponseEntity.notFound().build());
                //.orElseGet(() -> ResponseEntity.noContent().build());
        
    }
    
    // @requestbody significa que el cliente vendrá en el cuerpo de la petición
    // @Valid es para que sea obligatorio y que se pueda retornar el código de error correcto según se especificó
    //      en el modelo Cliente.java
    @PostMapping
    public ResponseEntity<ClienteDto> create(@Valid @RequestBody Cliente cliente){
        // retornaba el model Cliente
        //return new ResponseEntity<>(clienteService.create(cliente), HttpStatus.CREATED);
        
        return new ResponseEntity<>(modelMapper.map(clienteService.create(cliente), ClienteDto.class), HttpStatus.CREATED);
    }
    
    @PutMapping
    public ResponseEntity<ClienteDto> update(@Valid @RequestBody Cliente cliente){
        // Lo que se debe hacer primero es encontrar el cliente que se quiere modificar, ver si existe
        // En el caso de que el cliente fue encontrado, se lo recorre con el método map, que con una expresión lambda (->) 
        //      indicamos que cliente encontrado en este caso llamado cli responderá ok, y como cuerpo de la respuesta se actualiza
        //      y devuelve el cliente, porque el método update devuelve el objeto Cliente
        // En el caso de que no se lo encuentre orElseGet se indica o responde que no fue encontrado y que además construya la respuesta el método build()
        return (ResponseEntity<ClienteDto>) clienteService.findById(cliente.getIdcliente())
                .map(cli -> ResponseEntity.ok(modelMapper.map(clienteService.update(cliente), ClienteDto.class)))
                .orElseGet(() -> ResponseEntity.notFound().build());
    }
    
    @DeleteMapping("/{id}")
    public ResponseEntity<ClienteDto> delete(@PathVariable("id") Integer idcliente){
        // Muy parecido al update, con la diferencia que el ClienteService.delete no devuelve el Cliente eliminado
        // entonces se debe agregar una línea para devolverlo, y por eso luego de la operación delete devuelvo el cliente encontrado
        // y para agregar esa línea debo encerrar el bloque entre llaves '{}'. y cada línea terminar con ';'
        return (ResponseEntity<ClienteDto>) clienteService.findById(idcliente)
                .map(cli -> {
                            clienteService.delete(idcliente);
                            return ResponseEntity.ok(modelMapper.map(cli, ClienteDto.class));
                        })
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    // Si quiero por ejemplo usar parámetros así: http://localhost:6060/api/cliente/ronny?n1=3&n2=4
    @GetMapping("/ronny")
    public ResponseEntity<String> sumar(@RequestParam("n1") String num1, @RequestParam("n2") String num2){
        try {
            return ResponseEntity.ok("La suma es: " + (Double.parseDouble(num1) + Double.parseDouble(num2)));
        } catch (Exception e) {
            return ResponseEntity.badRequest().build();
        }
    }
    
    // ******* OJO que este es solo para explicar
    // solo para poner de ejemplo , de cuando no retorno algún valor
    public ResponseEntity<Object> deleteSinRetornarElCLiente(@PathVariable("id") Integer idcliente){
        // <Object> en vez de <Cliente>
        return (ResponseEntity<Object>) clienteService.findById(idcliente)
                .map(cli -> {
                            clienteService.delete(idcliente);
                            return ResponseEntity.ok().build(); // aquí cambia, porque no me retorna el cliente
                        })
                .orElseGet(() -> ResponseEntity.notFound().build());
    }
}

package com.satgy.ventas.controller;

import com.satgy.ventas.model.Bitacora;
import com.satgy.ventas.service.BitacoraService;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/bitacora")
public class BitacoraController {
    
    @Autowired
    private BitacoraService bitacoraService;
    
    @GetMapping
    public ResponseEntity<List<Bitacora>> findAll(){
        return ResponseEntity.ok(bitacoraService.findAll());
    }
    
    @GetMapping("/{id}")
    public ResponseEntity<Bitacora> findById(@PathVariable("id") Integer idbitacora){
        return (ResponseEntity<Bitacora>) bitacoraService.findById(idbitacora)
                .map(ResponseEntity::ok)
                .orElseGet(()->ResponseEntity.notFound().build());
    }
    
    @PostMapping
    public ResponseEntity<Bitacora> create(@Valid @RequestBody Bitacora bitacora){
        return new ResponseEntity<>(bitacoraService.create(bitacora), HttpStatus.CREATED);
    }
    
    @PutMapping
    public ResponseEntity<Bitacora> update(@Valid @RequestBody Bitacora bitacora){
        return (ResponseEntity<Bitacora>) bitacoraService.findById(bitacora.getIdbitacora())
                .map(t -> ResponseEntity.ok(bitacoraService.update(bitacora)))
                .orElseGet(()->ResponseEntity.notFound().build());
    }
    
    @DeleteMapping("/{id}")
    public ResponseEntity<Bitacora> delete (@PathVariable("id") Integer idbitacora){
        return (ResponseEntity<Bitacora>) bitacoraService.findById(idbitacora)
                .map(t -> {
                    bitacoraService.delete(idbitacora);
                    return ResponseEntity.ok(t);
                })
                .orElseGet(()->ResponseEntity.notFound().build());
    }
}

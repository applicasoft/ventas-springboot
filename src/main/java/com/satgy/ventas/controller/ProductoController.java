package com.satgy.ventas.controller;

import com.satgy.ventas.general.Lista;
import com.satgy.ventas.general.Respuesta;
import com.satgy.ventas.model.Producto;
import com.satgy.ventas.service.ProductoService;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/producto")
public class ProductoController {
    
    @Autowired
    private ProductoService productoService;
    
    @GetMapping("/pageable")
    public ResponseEntity<Respuesta> getAllPageable(
                        @RequestParam(defaultValue = "0") Integer pageNo, 
                        @RequestParam(defaultValue = "5") Integer pageSize,
                        @RequestParam(defaultValue = "idproducto") String sortBy) 
    {
        Respuesta respuesta = productoService.getAllPageable(pageNo, pageSize, sortBy);
        return ResponseEntity.ok(respuesta);
        
        //return new ResponseEntity<List<Producto>>(list, new org.springframework.http.HttpHeaders(), HttpStatus.OK);
    }
    
    @GetMapping("/nombrepageable")
    public ResponseEntity<Respuesta> getNombrePageable(
                        @RequestParam(defaultValue = "") String nombre, 
                        @RequestParam(defaultValue = "0") Integer pageNo, 
                        @RequestParam(defaultValue = "5") Integer pageSize,
                        @RequestParam(defaultValue = "idproducto") String sortBy) 
    {
        Respuesta respuesta = productoService.getNombrePageable(nombre, pageNo, pageSize, sortBy);
        return ResponseEntity.ok(respuesta);
        
        //return new ResponseEntity<List<Producto>>(list, new org.springframework.http.HttpHeaders(), HttpStatus.OK);
    }
    
    //http://localhost:6060/api/producto/par?nombre=a&min=20&max=30
    @GetMapping("/parametrospageable")
    public ResponseEntity<Respuesta> findParametrosPageable(
            @RequestParam(required = false) String nombre, 
            @RequestParam(required = false) String min, 
            @RequestParam(required = false) String max,
            @RequestParam(defaultValue = "0") Integer pageNo, 
            @RequestParam(defaultValue = "5") Integer pageSize)
    {
        Lista <String, String> l = new Lista();
        if (nombre != null) l.add("nombre", nombre);
        if (min != null) l.add("precio minimo", min);
        if (max != null) l.add("precio maximo", max);
        return ResponseEntity.ok(productoService.findParametrosPageable(l, pageNo, pageSize));
    }
    
    @GetMapping
    public ResponseEntity<List<Producto>> findAll(){
        //return ResponseEntity.ok(productoService.findAll());
        return ResponseEntity.ok(productoService.findEM());
    }
    
    //http://localhost:6060/api/producto/par?nombre=a&min=20&max=30
    @GetMapping("/par")
    public ResponseEntity<List<Producto>> findParametros(@RequestParam(required = false) String nombre, @RequestParam(required = false) String min, @RequestParam(required = false) String max){
        Lista <String, String> l = new Lista();
        if (nombre != null) l.add("nombre", nombre);
        if (min != null) l.add("precio minimo", min);
        if (max != null) l.add("precio maximo", max);
        return ResponseEntity.ok(productoService.findParametros(l));
    }
    
    @GetMapping("/{id}")
    public ResponseEntity<Producto> findById(@PathVariable("id") Integer idproducto){
        return (ResponseEntity<Producto>) productoService.findById(idproducto)
                .map(ResponseEntity::ok)
                .orElseGet(()->ResponseEntity.notFound().build());
    }
    
    @PostMapping
    public ResponseEntity<Producto> create(@Valid @RequestBody Producto producto){
        return new ResponseEntity<>(productoService.create(producto), HttpStatus.CREATED);
    }
    
    @PutMapping
    public ResponseEntity<Producto> update(@Valid @RequestBody Producto producto){
        return (ResponseEntity<Producto>) productoService.findById(producto.getIdproducto())
                .map(prod -> ResponseEntity.ok(productoService.update(producto)))
                .orElseGet(()->ResponseEntity.notFound().build());
    }
    
    @DeleteMapping("/{id}")
    public ResponseEntity<Producto> delete (@PathVariable("id") Integer idproducto){
        return (ResponseEntity<Producto>) productoService.findById(idproducto)
                .map(prod -> {
                    productoService.delete(idproducto);
                    return ResponseEntity.ok(prod);
                })
                .orElseGet(()->ResponseEntity.notFound().build());
    }
}

package com.satgy.ventas.controller;

import com.satgy.ventas.exception.ExceptionResponse;
import com.satgy.ventas.model.Persona;
import com.satgy.ventas.service.PersonaService;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/persona")
public class PersonaController {
    
    @Autowired
    private PersonaService personaService;
    
    @GetMapping
    public ResponseEntity<List<Persona>> findAll(){
        return ResponseEntity.ok(personaService.findAll());
    }
    
    @GetMapping("/{id}")
    public ResponseEntity<Persona> findById(@PathVariable("id") Integer idpersona){
        return (ResponseEntity<Persona>) personaService.findById(idpersona)
                .map(ResponseEntity::ok)
                .orElseGet(()->ResponseEntity.notFound().build());
    }
    
    @PostMapping
    public ResponseEntity<Persona> create(@Valid @RequestBody Persona persona){
        List<Persona> personas = personaService.findByNombre(persona.getNombre());
        
        ExceptionResponse exceptionResponse = new ExceptionResponse(new java.util.Date(), "La has cagado","Nombre duplicado");
        
        if (personas.size()>0) {
            //return ResponseEntity.status(HttpStatus.CONFLICT).build();
            return new ResponseEntity(exceptionResponse, HttpStatus.CONFLICT);
        }
        return new ResponseEntity<Persona>(personaService.create(persona), HttpStatus.CREATED);
    }
    
    @PutMapping
    public ResponseEntity<Persona> update (@Valid @RequestBody Persona persona){
        return (ResponseEntity<Persona>) personaService.findById(persona.getIdpersona())
                .map(per -> ResponseEntity.ok(personaService.update(persona)))
                .orElseGet(()->ResponseEntity.notFound().build());
    }
    
    @DeleteMapping("/{id}")
    public ResponseEntity<Persona> delete(@PathVariable("id") Integer idpersona){
        return (ResponseEntity<Persona>) personaService.findById(idpersona)
                .map(per -> {
                    personaService.delete(idpersona);
                    return ResponseEntity.ok(per);
                })
                .orElseGet(()->ResponseEntity.notFound().build());
    }
    
    
}

package com.satgy.ventas.controller;

import com.satgy.ventas.general.Cadenas;
import com.satgy.ventas.general.Par;
import com.satgy.ventas.model.Archivo;
import com.satgy.ventas.service.ArchivoService;
import com.satgy.ventas.service.FileStorageService;
import com.satgy.ventas.service.SecuenciaService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/archivo")
public class ArchivoController {

    private static final Logger logger = LoggerFactory.getLogger(ArchivoController.class);

    @Autowired
    private FileStorageService fileStorageService;
    
    @Autowired
    private ArchivoService archivoService;
    
    @Autowired
    private SecuenciaService secuenciaService;

    @PostMapping("/uploadFile")
    public Archivo uploadFile(@RequestParam("file") MultipartFile file) {
        String nombreOriginal = org.springframework.util.StringUtils.cleanPath(file.getOriginalFilename());
        String extension = Cadenas.getExtension(org.springframework.util.StringUtils.cleanPath(file.getOriginalFilename()));
        if (extension == null) extension = "";
        else extension = "." + extension;
        
        String nombre = secuenciaService.getValorStr(Par.secuenciaArchivo) + extension;
        String fileName = fileStorageService.storeFile(file, nombre);

        String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath()
                .path("/api/archivo/downloadFile/")
                .path(fileName)
                .toUriString();

        Archivo archivo = new Archivo(fileName, nombreOriginal, fileDownloadUri, file.getContentType(), file.getSize());
        secuenciaService.aumentar(Par.secuenciaArchivo);
        return archivoService.create(archivo);
    }

    @PostMapping("/uploadMultipleFiles")
    public List<Archivo> uploadMultipleFiles(@RequestParam("files") MultipartFile[] files) {
        return Arrays.asList(files)
                .stream()
                .map(file -> uploadFile(file))
                .collect(Collectors.toList());
    }

    @GetMapping("/downloadFile/{fileName:.+}")
    public ResponseEntity<Resource> downloadFile(@PathVariable String fileName, HttpServletRequest request) {
        // Load file as Resource
        Resource resource = fileStorageService.loadFileAsResource(fileName);
        String nombreOriginal = archivoService.buscaPorNombre(fileName).getNombreOriginal();
        if ( nombreOriginal == null ) nombreOriginal = fileName;

        // Try to determine file's content type
        String contentType = null;
        try {
            contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
        } catch (IOException ex) {
            logger.info("Could not determine file type.");
        }

        // Fallback to the default content type if type could not be determined
        if(contentType == null) {
            contentType = "application/octet-stream";
        }

        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(contentType))

                // así quedaría para que se decargue con el nombre de archivo que está almacenado
                //.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")

                // uso la palabra attachment para que se descargue el archivo
                //.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + nombreOriginal + "\"")

                // en mi caso no quiero descargar, por eso quito la palabra attachment
                .header(HttpHeaders.CONTENT_DISPOSITION, "filename=\"" + nombreOriginal + "\"")
                .body(resource);
    }

    @GetMapping
    public ResponseEntity<List<Archivo>> findAll(){
        List<Archivo> archivos = archivoService.findAll();
        return ResponseEntity.ok(archivos);
    }
    
    @DeleteMapping("/{id}")
    public ResponseEntity<Archivo> delete(@PathVariable("id") Integer idarchivo){
        // no está funcionando el método archivoService.findById
        /*return (ResponseEntity<Archivo>) archivoService.findById(idarchivo)
                .map(arch -> {
                            archivoService.delete(idarchivo);
                            return ResponseEntity.ok(arch);
                        })
                .orElseGet(() -> ResponseEntity.notFound().build());
                */
        
        Archivo a = archivoService.buscaPorId(idarchivo);
        if (a != null) {
            archivoService.delete(idarchivo);
            fileStorageService.deleteFileAsResource(a.getNombre());
            return ResponseEntity.ok(a);
        }
        else return ResponseEntity.notFound().build();
    }
}

package com.satgy.ventas.dto;

import com.satgy.ventas.model.Cliente;
import com.satgy.ventas.model.Detalle;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class VentaDto {

    private Integer idventa;
    private Date fecha;
    private Cliente cliente;
    private List<Detalle> detalles;
    
    public VentaDto eliminaPadreDeHijos(){
        if (detalles != null) {
            List<Detalle> detallesNuevo = new ArrayList<>();
            detalles.forEach(d -> {
                detallesNuevo.add(d.eliminaPadre());
            });
            detalles = detallesNuevo;
        }
        return this;
    }
    
    public VentaDto eliminaHijos(){
        if (detalles != null) {
            detalles = null;
        }
        return this;
    }

    public Integer getIdventa() {
        return idventa;
    }

    public void setIdventa(Integer idventa) {
        this.idventa = idventa;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public List<Detalle> getDetalles() {
        return detalles;
    }

    public void setDetalles(List<Detalle> detalles) {
        this.detalles = detalles;
    }
}